﻿using SmartTreasury.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTreasury.DataGeneration
{
    public class DataGenerator
    {
        private readonly Random _random;

        public DataGenerator()
        {
            _random = new Random();
        }

        public IEnumerable<PaymentApplication> GenerateApplications(DateTime startDate, DateTime endDate, 
                                                                    int count,
                                                                    decimal minSum, decimal maxSum)
        {
            foreach (var i in Enumerable.Range(0, count))
            {
                
                yield return new PaymentApplication()
                {
                    StartDate = startDate,
                    DeadlineDate = GetRandomDateInRange(startDate, endDate),
                    TreasureDate = null,
                    Sum = GetRandomSum(minSum, maxSum),
                    Source = PaymentSource.RandomGenerated
                };
            }
        }

        public IEnumerable<ReceivedPayment> GenerateReceives(DateTime startDate, DateTime endDate, 
                                                                int count,
                                                                decimal minSum, decimal maxSum)
        {
            foreach (var i in Enumerable.Range(0, count))
            {

                yield return new ReceivedPayment()
                {
                    ReceiveDate = GetRandomDateInRange(startDate, endDate),
                    PaymentSum = GetRandomSum(minSum, maxSum),
                    Source = PaymentSource.RandomGenerated
                };
            }
        }

        private decimal GetRandomSum(decimal minSum, decimal maxSum)
        {
            return _random.Next((int)minSum, (int)maxSum);
        }

        private DateTime GetRandomDateInRange(DateTime start, DateTime end)
        {
            int timestampStart = (int)(new DateTimeOffset(start).ToUnixTimeSeconds());
            int timestampEnd = (int)(new DateTimeOffset(end).ToUnixTimeSeconds());

            int randomTimestamp = _random.Next(timestampStart, timestampEnd);

            return (new DateTimeOffset(1970, 01, 01, 0, 0, 0, new TimeSpan())).AddSeconds(randomTimestamp).Date;
        }
    }
}
