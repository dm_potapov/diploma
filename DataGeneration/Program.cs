﻿using MongoDB.Driver;
using Newtonsoft.Json;
using SmartTreasury.Models;
using SmartTreasury.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTreasury.DataGeneration
{
    class Program
    {
        static void Main(string[] args)
        {
            // Инициализация
            DateTime startDate = DateTime.Parse("2017-01-01");
            DateTime endDate = DateTime.Parse("2017-12-31");
            int applicationsCount = 100;
            int receivesCount = 10;
            decimal minAppSum = 1000;
            decimal maxAppSum = 10000;
            decimal minReceiveSum = 10000;
            decimal maxReceiveSum = 200000;

            DataGenerator dataGenerator = new DataGenerator();

            // Генерация
            var applications = dataGenerator.GenerateApplications(startDate, endDate,
                                                                 applicationsCount,
                                                                 minAppSum, maxAppSum);
            var receives = dataGenerator.GenerateReceives(startDate, endDate,
                                                          receivesCount,
                                                          minReceiveSum, maxReceiveSum);
            var creditLines = new List<CreditLine>()
            {
                new CreditLine()
                {
                    AvailabilityStartDate = DateTime.Parse("2017-01-01"),
                    AvailabilityEndDate = DateTime.Parse("2017-12-31"),
                    MinDuration = 30,
                    MaxDuration = 366,
                    MinSum = 10000,
                    MaxSum = 1000000,
                    AlreadyGivenSum = 0,
                    IsRenewable = true,
                    Percent = 13,
                    PercentPaymentPeriod = PaymentPeriod.Year
                }
            };

            // Сохранение в json-документы
            SaveIEnumerableToMongo(applications);
            SaveIEnumerableToMongo(receives);
            SaveIEnumerableToMongo(creditLines);
        }

        static void SaveIEnumerableToMongo<T>(IEnumerable<T> enumerable) where T : IEntity
        {
            var mongoDbRepository = new MongoDbRepository<T>(new MongoClient("mongodb://localhost:27017"),
                                                          "smartTreasury");
            foreach (T item in enumerable)
            {
                mongoDbRepository.Save(item);
            }
        }

        static void SaveIEnumerableToJson<T>(IEnumerable<T> enumerable, string path)
        {
            File.WriteAllText(path, JsonConvert.SerializeObject(enumerable), Encoding.UTF8);
        }
    }
}
