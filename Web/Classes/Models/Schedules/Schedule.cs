﻿using SmartTreasury.Models;
using System;
using System.Collections.Generic;

namespace SmartTreasury.Classes.Models.Schedules
{
    /// <summary>
    /// Календарь платежей
    /// </summary>
    public class Schedule : IEntity
    {
        /// <summary>
        /// Id календаря
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Id пользователя, создавшего календарь
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Список платежей по данному календарю
        /// </summary>
        public IList<ScheduleItem> ScheduleItems { get; set; }
    }
}
