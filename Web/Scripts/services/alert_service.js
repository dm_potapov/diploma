﻿angular.module('smartTreasury')
.service('AlertService', function ($rootScope, $timeout) {

    $rootScope.alerts = [];
    $rootScope.closeAlert = function (index) {
        $rootScope.alerts.splice(index, 1);
    }

    return {
        showMessage: function (type, message, interval) {
            var index = $rootScope.alerts.push({ 'type': type, 'message': message }) - 1;
            $timeout(function () { $rootScope.closeAlert(index) }, interval);
        }
    }

});