﻿angular.module('smartTreasury')
.service('ScheduleService', function ($http, DateTransformService, AlertService) {
    return {
        GetSchedule: function () {
            return $http.get('/api/schedule/latest').then(function (resp) {
                DateTransformService.transformMultipleDates(resp.data, "date", "d-m-y");
                return resp.data;
            }, window.onAjaxError);
        },
        GetScheduleByDates: function ($ctrlScope, from, to) {
            if (from == undefined) {
                from = new Date('1000-01-01');
            }
            if (to == undefined) {
                to = new Date('9999-12-31');
            }
            var fromTransformed = DateTransformService.transformDate(from, "y-m-d");
            var toTransformed = DateTransformService.transformDate(to, "y-m-d");
            return $http.get('/api/schedule/latest/' + fromTransformed + '/' + toTransformed)
                .then(function (resp) {
                    DateTransformService.transformMultipleDates(resp.data, "date", "d-m-y");
                    $ctrlScope.schedule = resp.data;
                    return resp.data;
                }, window.onAjaxError);
        },
        FormSchedule: function ($ctrlScope) {
            return $http.post('/api/schedule/').then(function (resp) {
                AlertService.showMessage('success', 'Календарь успешно рассчитан', 5000);
                DateTransformService.transformMultipleDates(resp.data, "date", "d-m-y");
                $ctrlScope.schedule = resp.data;
                return resp.data;
            }, window.onAjaxError);
        }
    }
});