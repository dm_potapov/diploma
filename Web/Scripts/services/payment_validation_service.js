﻿angular.module('smartTreasury')
.service('PaymentValidationService', function () {
    function paymentPartsSumEqualsPaymentSum(payment) {
        var sum = 0;
        for (i in payment.paymentParts) {
            sum += payment.paymentParts[i].sum;
        }
        return payment.sum === sum;
    }

    function paymentPartsDatesLessThenPaymentStartDate(payment) {
        for (i in payment.paymentParts) {
            if (payment.paymentParts[i].date <= payment.createDate) {
                return false;
            }
        }
        return true;
    }

    return {
        validate: function (payment) {
            switch (true) {
                case (payment.pType == 2 && !paymentPartsSumEqualsPaymentSum(payment)):
                    return "Сумма всех платежей по графику должна быть равна общей сумме платежного обязательства!";

                case (!paymentPartsDatesLessThenPaymentStartDate(payment)):
                    return "Дата создания не может быть больше крайнего срока!";

                case (payment.sum < payment.sumRemained):
                    return "Остаток не может быть меньше общей суммы!";

                default:
                    return 'ok';
            }
        }
    }
})