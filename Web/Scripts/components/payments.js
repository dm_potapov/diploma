﻿angular.module('smartTreasury')
.component('payments', {
    bindings: { payments: '=' },
    templateUrl: "templates/payments.html",
    controller: function (PaymentsService) {
        var $ctrl = this;
        $ctrl.getPaymentType = function (type) {
            switch (parseInt(type)) {
                case 0:
                    return "Единоразовый";

                case 1:
                    return "Ежемесячный";

                case 2:
                    return "По графику";

                default:
                    return "Не задано (???)";
            }
        }
        $ctrl.removePayment = function (guid, index) {
            if (confirm("Вы уверены?")) {
                PaymentsService.RemovePayment(guid);
                $ctrl.payments.splice(index, 1);
            }
        }
    }
})