﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTreasury.Models
{
    /// <summary>
    /// Денежное поступление
    /// </summary>
    public class ReceivedPayment : IEntity
    {
        public Guid Id { get; set; }

        /// <summary>
        /// Дата поступления
        /// </summary>
        public DateTime ReceiveDate { get; set; }

        /// <summary>
        /// Сумма поступления
        /// </summary>
        public decimal PaymentSum { get; set; }

        /// <summary>
        /// Источник поступления платежа (кредит, генерация, etc.)
        /// </summary>
        public PaymentSource Source { get; set; }
    }
}
