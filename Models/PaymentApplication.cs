﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTreasury.Models
{
    /// <summary>
    /// Заявка на платёж
    /// </summary>
    public class PaymentApplication : IEntity
    {
        public Guid Id { get; set; }

        /// <summary>
        /// Дата начала периода оплаты
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Крайний срок оплаты
        /// </summary>
        public DateTime DeadlineDate { get; set; }

        /// <summary>
        /// Казначейская дата
        /// </summary>
        public DateTime? TreasureDate { get; set; }

        /// <summary>
        /// Сумма платежа
        /// </summary>
        public decimal Sum { get; set; }

        /// <summary>
        /// Источник поступления платежа (кредит, генерация, etc.)
        /// </summary>
        public PaymentSource Source { get; set; }
    }
}
