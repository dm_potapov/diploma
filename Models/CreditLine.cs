﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTreasury.Models
{
    /// <summary>
    /// Кредитная линия
    /// </summary>
    public class CreditLine : IEntity
    {
        public Guid Id { get; set; }

        /// <summary>
        /// Дата начала доступности данной кредитной линии
        /// </summary>
        public DateTime AvailabilityStartDate { get; set; }

        /// <summary>
        /// Дата окончания доступности данной кредитной линии
        /// </summary>
        public DateTime AvailabilityEndDate { get; set; }

        /// <summary>
        /// Минимальная длительность заёма в днях
        /// </summary>
        public int MinDuration { get; set; }

        /// <summary>
        /// Максимальная длительность заёма в днях
        /// </summary>
        public int MaxDuration { get; set; }

        /// <summary>
        /// Минимальная сумма
        /// </summary>
        public decimal MinSum { get; set; }

        /// <summary>
        /// Максимальная сумма
        /// </summary>
        public decimal MaxSum { get; set; }

        /// <summary>
        /// Является ли сумма возобновляемой
        /// </summary>
        public bool IsRenewable { get; set; }

        /// <summary>
        /// Уже выданное количество средств
        /// </summary>
        public decimal AlreadyGivenSum { get; set; }

        /// <summary>
        /// Процентная ставка
        /// </summary>
        public double Percent { get; set; }

        /// <summary>
        /// Период выплаты процентов
        /// </summary>
        public PaymentPeriod PercentPaymentPeriod { get; set; }
    }
}
