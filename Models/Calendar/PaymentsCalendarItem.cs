﻿using SmartTreasury.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models.Calendar
{
    public class PaymentsCalendarItem
    {
        public DateTime Date { get; private set; }

        public decimal Balance
        {
            get
            {
                return Receives.Sum(r => r.PaymentSum) - Applications.Sum(a => a.Sum);
            }
        }

        public IEnumerable<ReceivedPayment> Receives { get; private set; }

        public IEnumerable<PaymentApplication> Applications { get; private set; }

        public PaymentsCalendarItem(DateTime date, IEnumerable<ReceivedPayment> receives,
                                    IEnumerable<PaymentApplication> applications)
        {
            if (date == null) throw new ArgumentNullException();
            if (receives == null) throw new ArgumentNullException();
            if (applications == null) throw new ArgumentNullException();

            Date = date;
            Receives = receives;
            Applications = applications;
        }
    }
}
