﻿namespace SmartTreasury.Models
{
    /// <summary>
    /// Период выплаты процентов по кредиту/депозиту
    /// </summary>
    public enum PaymentPeriod
    {
        /// <summary>
        /// Месяц
        /// </summary>
        Month = 4,
        /// <summary>
        /// Квартал
        /// </summary>
        Quarter = 5,
        /// <summary>
        /// Год
        /// </summary>
        Year = 7
    }
}