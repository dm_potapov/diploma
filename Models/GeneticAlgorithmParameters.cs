﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTreasury.Models
{
    /// <summary>
    /// Параметры генетического алгоритма
    /// </summary>
    public class GeneticAlgorithmParameters
    {
        /// <summary>
        /// Величина популяции
        /// </summary>
        public int PopulationCount { get; set; }

        /// <summary>
        /// Количество итераций
        /// </summary>
        public int IterationsCount { get; set; }

        /// <summary>
        /// Ожидаемое значение фитнес-функции
        /// </summary>
        public decimal ExpectedFitnessFunctionValue { get; set; }
    }
}
