﻿using Models.Calendar;
using MongoDB.Driver;
using SmartTreasury.Models;
using SmartTreasury.Models.SourceData;
using SmartTreasury.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SmartTreasury.Main
{
    public class PaymentsCalendarMakingProcess
    {
        private readonly CreditCalculator _creditCalculator;
        private readonly DepositCalculator _depositCalculator;
        private readonly GeneticAlgorithm _algorithm;
        private readonly IRepository<PaymentApplication> _applications;
        private readonly IRepository<BankAccount> _bankAccounts;
        private readonly IRepository<ReceivedPayment> _receives;
        private readonly IRepository<CreditLine> _creditLines;
        private readonly IRepository<DepositBill> _depositBills;
        private readonly IRepository<PaymentsCalendarCalculationResult> _results;

        public PaymentsCalendarMakingProcess(CreditCalculator creditCalculator, GeneticAlgorithm algorithm,
                                             IRepository<PaymentApplication> applications,
                                             IRepository<BankAccount> bankAccounts,
                                             IRepository<ReceivedPayment> receives,
                                             IRepository<CreditLine> creditLines,
                                             IRepository<DepositBill> depositBills,
                                             IRepository<PaymentsCalendarCalculationResult> results)
        {
            if (creditCalculator == null) throw new ArgumentNullException();
            if (algorithm == null) throw new ArgumentNullException();

            _creditCalculator = creditCalculator;
            _algorithm = algorithm;
            _applications = applications;
            _bankAccounts = bankAccounts;
            _receives = receives;
            _creditLines = creditLines;
            _depositBills = depositBills;
            _results = results;
        }

        public Guid DoProcess(DateTime dateStart, DateTime dateEnd, int iterationsCount, int populationSize, Guid sessionId)
        {
            var appsFromPeriod = _applications.Where(p => p.DeadlineDate > dateStart
                                                                              && p.StartDate < dateEnd).ToList();
            var receivesFromPeriod = GetCollection<ReceivedPayment>().Where(p => p.ReceiveDate > dateStart
                                                                        && p.ReceiveDate < dateEnd).ToList();

            var accsToList = _bankAccounts.ToList();
            foreach(var app in appsFromPeriod)
            {
                if (!app.TreasureDate.HasValue)
                {
                    app.TreasureDate = app.DeadlineDate;
                }
            }

            Console.WriteLine(FitnessFunction.Calculate(accsToList, appsFromPeriod, receivesFromPeriod, dateStart, dateEnd));

            var optimizedApplications = _algorithm.GetOptimizedPaymentApplications(new GeneticAlgorithmParameters()
            {
                DateStart = dateStart,
                DateEnd = dateEnd,
                BankAccounts = accsToList,
                Receives = receivesFromPeriod,
                PaymentApplications = appsFromPeriod,
                IterationsCount = iterationsCount,
                PopulationCount = populationSize,
                SessionId = sessionId
            });

            Console.WriteLine(FitnessFunction.Calculate(accsToList, optimizedApplications, receivesFromPeriod, dateStart, dateEnd));

            IEnumerable<TakenCredit> credits = AddCreditsIfNeeded(dateStart, dateEnd, ref optimizedApplications, ref receivesFromPeriod);

            PaymentsCalendarCalculationResult calendar = new PaymentsCalendarCalculationResult(optimizedApplications, receivesFromPeriod,
                                                           credits, new List<GivenDeposit>());

            _results.Save(calendar);

            return calendar.Id;
        }

        private ICollection<T> GetCollection<T>() where T : IEntity
        {
            var mongoDbRepository = new MongoDbRepository<T>(new MongoClient("mongodb://localhost:27017"),
                                                          "smartTreasury");
            return mongoDbRepository.ToList();
        }

        private IEnumerable<TakenCredit> AddCreditsIfNeeded
                            (DateTime periodStart,
                             DateTime periodEnd,
                             ref List<PaymentApplication> applications,
                             ref List<ReceivedPayment> receivedPayments)
        {
            var orderedReceives = receivedPayments.OrderBy(r => r.ReceiveDate).ToList();
            var prevDate = periodStart;
            var takenCreditsInfo = new List<TakenCredit>();
            foreach (var payment in orderedReceives)
            {
                var appsSum = applications.Where(a => a.DeadlineDate >= prevDate && a.DeadlineDate <= payment.ReceiveDate)
                                          .Sum(a => a.Sum);
                if (appsSum > payment.Sum)
                {
                    var creditCalcResult = _creditCalculator.TryToGetCredit(periodStart, appsSum - payment.Sum,
                                                               (int)(payment.ReceiveDate - prevDate).TotalDays);
                    if (creditCalcResult.Status == CreditCalculateStatus.CreditIsAvailable)
                    {
                        receivedPayments.Add(new ReceivedPayment
                        {
                            Id = Guid.NewGuid(),
                            Sum = appsSum - payment.Sum,
                            ReceiveDate = periodStart,
                            Source = PaymentSource.Credit
                        });
                        takenCreditsInfo.Add(creditCalcResult.CreditInfo);
                        applications = applications.Concat(creditCalcResult.CreditParts).ToList();
                    }
                }
                prevDate = payment.ReceiveDate;
            }

            return takenCreditsInfo;
        }

        private IEnumerable<GivenDeposit> AddDepositsIfNeeded
                            (DateTime periodStart,
                             DateTime periodEnd,
                             ref ICollection<PaymentApplication> applications,
                             ref ICollection<ReceivedPayment> receivedPayments)
        {
            return new List<GivenDeposit>();
        }
    }
}
