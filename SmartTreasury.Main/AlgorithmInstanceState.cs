﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTreasury.Main
{
    public class AlgorithmInstanceState
    {
        public int Counter { get; set; }
        public bool CancellationToken { get; set; }
    }
}
