﻿using SmartTreasury.Models;
using SmartTreasury.Models.SourceData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTreasury.Main
{
    /// <summary>
    /// Класс, расчитывающий остатки на счёте на основе данных о поступлениях и расходах
    /// </summary>
    public class BankAccountResidueCalculator
    {
        public static IEnumerable<BankAccountResidue> Calculate(DateTime periodStart,
                                                        DateTime periodEnd,
                                                        IEnumerable<PaymentApplication> paymentApplications,
                                                        IEnumerable<ReceivedPayment> receivedPayments)
        {
            List<BankAccountResidue> residues = new List<BankAccountResidue>();

            // Вычисляем общий остаток, который был до начала периода вычисления остатков
            decimal startResidueSum = receivedPayments.Where(pa => pa.ReceiveDate < periodStart)
                                                      .Sum(pa => pa.Sum) -
                                                       paymentApplications.Where(pa => pa.TreasureDate < periodStart)
                                                         .Sum(pa => pa.Sum);

            decimal endResidueSum = 0;

            for (; periodStart <= periodEnd;
                   periodStart = periodStart.AddDays(1))
            {
                endResidueSum = startResidueSum + 
                                (
                                    receivedPayments.Where(pa => pa.ReceiveDate.Date.Date == periodStart.Date).Sum(pa => pa.Sum) - 
                                    paymentApplications.Where(pa => pa.TreasureDate.Value.Date.Date == periodStart.Date).Sum(pa => pa.Sum)
                                 );

                var residue = new BankAccountResidue()
                {
                    Id = Guid.NewGuid(),
                    Date = periodStart,
                    SumStart = startResidueSum,
                    SumEnd = endResidueSum
                };
                residues.Add(residue);

                startResidueSum = endResidueSum;
            }

            return residues;
        }
    }
}
