﻿using SmartTreasury.Models;
using SmartTreasury.Models.SourceData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTreasury.Main
{
    public class PaymentsCalendar
    {
        public Dictionary<string, IEnumerable<BankAccountResidue>> Residues { get; private set; }

        public PaymentsCalendar(IEnumerable<BankAccount> accounts, 
                                IEnumerable<PaymentApplication> applications, 
                                IEnumerable<ReceivedPayment> receives, 
                                DateTime dateStart, DateTime dateEnd)
        {
            Residues = new Dictionary<string, IEnumerable<BankAccountResidue>>();

            foreach (var acc in accounts)
            {
                Residues.Add(acc.AccountNumber, BankAccountResidueCalculator.Calculate(dateStart, dateEnd,
                                                            applications.Where(a => a.BankAccountId == acc.Id).ToList(),
                                                            receives.Where(a => a.BankAccountId == acc.Id).ToList()));
            }
        }
    }
}
