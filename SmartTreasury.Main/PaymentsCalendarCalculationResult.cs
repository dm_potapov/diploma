﻿using Models.Calendar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections;
using System.Linq.Expressions;
using SmartTreasury.Models;

namespace SmartTreasury.Main
{
    public class PaymentsCalendarCalculationResult : IEntity
    {
        public Guid Id { get; set; }

        public IEnumerable<PaymentApplication> Applications { get; private set; }

        public IEnumerable<ReceivedPayment> Receives { get; private set; }

        public IEnumerable<TakenCredit> TakenCredits { get; private set; }

        public IEnumerable<GivenDeposit> GivenDeposits { get; private set; }

        public PaymentsCalendarCalculationResult(IEnumerable<PaymentApplication> applications,
                                IEnumerable<ReceivedPayment> receives,
                                IEnumerable<TakenCredit> takenCredits,
                                IEnumerable<GivenDeposit> givenDeposits)
        {
            if (applications == null) throw new ArgumentNullException();
            if (receives == null) throw new ArgumentNullException();
            if (takenCredits == null) throw new ArgumentNullException();
            if (givenDeposits == null) throw new ArgumentNullException();

            Applications = applications;
            Receives = receives;
            TakenCredits = takenCredits;
            GivenDeposits = givenDeposits;
            Id = Guid.NewGuid();
        }
    }
}
