﻿using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SmartTreasury.Models;
using SmartTreasury.Shared.Logic;
using SmartTreasury.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartTreasury.Main
{
    class Program
    {
        private const int portListen = 12345;
        private const int portSend = 23456;

        private static Dictionary<Guid, Task<PaymentsCalendarCalculationResult>> _algorithmInstances = 
                                new Dictionary<Guid, Task<PaymentsCalendarCalculationResult>>();

        static void Main(string[] args)
        {

            //SocketMessagesSender sender = new SocketMessagesSender();
            //while (true)
            //{
            //    var data = sender.WaitForMessage(portListen);

            //    var dataAsObject = JsonConvert.DeserializeObject<JObject>(data);

            //    object answer;

            //    switch (dataAsObject["action"].ToString())
            //    {
            //        case "Start":
            //            var newSessionId = Guid.NewGuid();
            //            var dateStart = DateTime.Parse(dataAsObject["data"]["DateStart"].ToString());
            //            var dateEnd = DateTime.Parse(dataAsObject["data"]["DateEnd"].ToString());
            //            _algorithmInstances.Add(newSessionId, 
            //                                    new Task<PaymentsCalendar>(() => PaymentsCalendarMakingProcess.DoProcess(dateStart, dateEnd)));
            //            _algorithmInstances[newSessionId].Start();
            //            answer = new
            //            {
            //                sessionId = newSessionId
            //            };
            //            break;

            //        case "Check":
            //            var sessionId = Guid.Parse(dataAsObject["data"]["SessionId"].ToString());
            //            bool isAlgorithmCompleted = _algorithmInstances[sessionId].IsCompleted;
            //            answer = new
            //            {
            //                IsCompleted = isAlgorithmCompleted,
            //                Result = (isAlgorithmCompleted) ? _algorithmInstances[sessionId].Result
            //                                                : null
            //            };
            //            break;

            //        default:
            //            throw new ArgumentException("Wrong action!");
            //    }

            //    sender.Send(JsonConvert.SerializeObject(answer), portSend);
            //}
        }
    }
}
