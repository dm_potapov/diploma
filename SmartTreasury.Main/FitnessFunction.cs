﻿using SmartTreasury.Models;
using SmartTreasury.Models.SourceData;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SmartTreasury.Main
{
    public class FitnessFunction
    {
        /// <summary>
        /// Вычисление фитнес-функции - сумма отрицательных остатков по всем счетам
        /// (чем ближе к нулю, тем лучше)
        /// </summary>
        /// <returns></returns>
        public static decimal Calculate(IEnumerable<BankAccount> bankAccounts,
                            IEnumerable<PaymentApplication> paymentApplications,
                            IEnumerable<ReceivedPayment> receivedPayments,
                            DateTime periodStart,
                            DateTime periodEnd)
        {
            decimal value = 0;
            foreach(var acc in bankAccounts)
            {
                value += BankAccountResidueCalculator.Calculate(periodStart, periodEnd,
                                                   paymentApplications.Where(p => p.BankAccountId == acc.Id).ToList(),
                                                   receivedPayments.Where(p => p.BankAccountId == acc.Id).ToList())
                                                   .Where(r => r.SumEnd < 0).Sum(r => r.SumEnd);
            }

            return value;
        }
    }
}
