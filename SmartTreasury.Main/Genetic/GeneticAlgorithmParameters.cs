﻿using SmartTreasury.Models.SourceData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTreasury.Models
{
    /// <summary>
    /// Параметры генетического алгоритма
    /// </summary>
    public class GeneticAlgorithmParameters
    {
        /// <summary>
        /// Величина популяции
        /// </summary>
        public int PopulationCount { get; set; }

        /// <summary>
        /// Количество итераций
        /// </summary>
        public int IterationsCount { get; set; }

        /// <summary>
        /// Период начала оптимизации
        /// </summary>
        public DateTime DateStart { get; set; }

        /// <summary>
        /// Период окончания оптимизации
        /// </summary>
        public DateTime DateEnd { get; set; }

        /// <summary>
        /// Заявки на платежи
        /// </summary>
        public IEnumerable<PaymentApplication> PaymentApplications { get; set; }

        /// <summary>
        /// Поступающие платежи
        /// </summary>
        public IEnumerable<ReceivedPayment> Receives { get; set; }

        /// <summary>
        /// Счета
        /// </summary>
        public IEnumerable<BankAccount> BankAccounts { get; set; }

        public Guid SessionId { get; internal set; }
    }
}
