﻿using SmartTreasury.Models;
using SmartTreasury.Models.SourceData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChromosomesSet = System.Collections.Generic.List<SmartTreasury.Models.PaymentApplication>;

namespace SmartTreasury.Main.Genetic
{   
    using ChromosomesSetsList = List<ChromosomesSet>;

    /// <summary>
    /// Класс, генерирующий хромосомы для генетического алгоритма
    /// (в данном случае - заявки на платежи со случайно выбранными датами и счетами для оплаты)
    /// </summary>
    public class ChromosomesGenerator
    {
        private readonly Random _random;

        public ChromosomesGenerator(Random random)
        {
            if (random == null) throw new ArgumentNullException();

            _random = random;
        }

        public ChromosomesSetsList GeneratePaymentApplcationsVariants(IEnumerable<PaymentApplication> applications,
                                                                 IEnumerable<BankAccount> bankAccounts,
                                                                 DateTime dateStart,
                                                                 DateTime dateEnd,
                                                                 int count)
        {
            ChromosomesSetsList result = new ChromosomesSetsList();

            for (int i = 0; i < count; i++)
            {
                var chromosomesSet = new ChromosomesSet();
                foreach (var application in applications)
                {
                    chromosomesSet.Add(GenerateOnePaymentApplicationVariant(application, bankAccounts,
                                                                            dateStart, dateEnd));
                }
                result.Add(chromosomesSet);
            }

            return result;
        }

        public PaymentApplication GenerateOnePaymentApplicationVariant(PaymentApplication application,
                                                                 IEnumerable<BankAccount> bankAccounts,
                                                                 DateTime dateStart,
                                                                 DateTime dateEnd)
        {
            var newApp = new PaymentApplication()
            {
                Id = application.Id,
                DeadlineDate = application.DeadlineDate,
                IsFullPay = application.IsFullPay,
                OperationType = application.OperationType,
                OrganizationId = application.OrganizationId,
                Priority = application.Priority,
                StartDate = application.StartDate,
                Sum = application.Sum,
                Source = application.Source
            };

            newApp.TreasureDate = GetRandomDateInRange(application.StartDate, application.DeadlineDate);
            newApp.BankAccountId = GetRandomBankAccount(bankAccounts).Id;

            return newApp;
        }

        // TODO: отбирать исходя из какой-либо вероятности (например, количество денег на счёте в опр. день)
        private BankAccount GetRandomBankAccount(IEnumerable<BankAccount> bankAccounts)
        {
            var randomIndex = _random.Next(0, bankAccounts.Count());

            return bankAccounts.ToArray()[randomIndex];
        }

        private DateTime GetRandomDateInRange(DateTime start, DateTime end)
        {
            int timestampStart = (int)(new DateTimeOffset(start).ToUnixTimeSeconds());
            int timestampEnd = (int)(new DateTimeOffset(end).ToUnixTimeSeconds());

            int randomTimestamp = _random.Next(timestampStart, timestampEnd);

            var generatedDate = (new DateTimeOffset(1970, 01, 01, 0, 0, 0, new TimeSpan())).AddSeconds(randomTimestamp).Date;

            return DateTime.SpecifyKind(generatedDate, DateTimeKind.Utc);
        }
    }
}
