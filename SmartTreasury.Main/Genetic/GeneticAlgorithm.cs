﻿using SmartTreasury.Main.Genetic;
using SmartTreasury.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChromosomesSet = System.Collections.Generic.List<SmartTreasury.Models.PaymentApplication>;

namespace SmartTreasury.Main
{
    using Models.SourceData;
    using ChromosomesSetsList = List<ChromosomesSet>;

    /// <summary>
    /// Реализация генетического алгоритма для оптимизации расположения заявок на платежи
    /// </summary>
    public class GeneticAlgorithm
    {
        private readonly ChromosomesGenerator _generator;
        private readonly Random _random;
        private readonly Dictionary<Guid, AlgorithmInstanceState> _states;

        public GeneticAlgorithm(ChromosomesGenerator generator, Random random, Dictionary<Guid, AlgorithmInstanceState> states)
        {
            if (generator == null) throw new ArgumentNullException();
            if (random == null) throw new ArgumentNullException();
            if (states == null) throw new ArgumentNullException();

            _generator = generator;
            _random = random;
            _states = states;
            
        }

        public ChromosomesSet GetOptimizedPaymentApplications(GeneticAlgorithmParameters parameters)
        {
            ChromosomesSetsList sets = _generator.GeneratePaymentApplcationsVariants(parameters.PaymentApplications,
                                                                                     parameters.BankAccounts,
                                                                                     parameters.DateStart,
                                                                                     parameters.DateEnd,
                                                                                     parameters.PopulationCount);

            Comparison<ChromosomesSet> sortCriteria = new Comparison<ChromosomesSet>((x, y) =>
            {
                var firstValue = FitnessFunction.Calculate(parameters.BankAccounts, x, parameters.Receives, parameters.DateStart,
                                                           parameters.DateEnd);
                var secondValue = FitnessFunction.Calculate(parameters.BankAccounts, y, parameters.Receives, parameters.DateStart,
                                                           parameters.DateEnd);
                if (firstValue > secondValue)
                {
                    return 1;
                }
                else if (firstValue < secondValue)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            });

            for (; _states[parameters.SessionId].Counter < parameters.IterationsCount && !_states[parameters.SessionId].CancellationToken; 
                _states[parameters.SessionId].Counter++)
            {
                // Производим кроссовер
                sets = Crossover(sets);

                // Производим мутации
                Mutate(ref sets, parameters.BankAccounts, parameters.DateStart, parameters.DateEnd);

                // Отбор методом турнирной селекции (вроде бы)
                sets = SelectChromosomes(sets, parameters.BankAccounts, parameters.Receives, parameters.DateStart, parameters.DateEnd);
            }

            sets.Sort(sortCriteria);

            return sets[0];
        }

        private ChromosomesSetsList SelectChromosomes(ChromosomesSetsList sets, 
                                                      IEnumerable<BankAccount> bankAccounts,
                                                      IEnumerable<ReceivedPayment> receives, 
                                                      DateTime dateStart, DateTime dateEnd)
        {
            ChromosomesSetsList selectedSets = new ChromosomesSetsList();

            //var ffs = sets.Select(s => FitnessFunction
            //                                .Calculate(bankAccounts, s, receives, dateStart, dateEnd))
            //                                .ToList();

            while (sets.Count != 0)
            {
                int firstCandidateIndex = _random.Next(0, sets.Count);
                int secondCandidateIndex = _random.Next(0, sets.Count);

                while (firstCandidateIndex == secondCandidateIndex)
                {
                    secondCandidateIndex = _random.Next(0, sets.Count);
                }

                ChromosomesSet firstCandidate = sets[firstCandidateIndex];
                ChromosomesSet secondCandidate = sets[secondCandidateIndex];

                decimal firstFitnessFunctionValue = FitnessFunction.Calculate(bankAccounts, firstCandidate, receives, dateStart, dateEnd);
                decimal secondFitnessFunctionValue = FitnessFunction.Calculate(bankAccounts, firstCandidate, receives, dateStart, dateEnd);

                selectedSets.Add((firstFitnessFunctionValue > secondFitnessFunctionValue) ? firstCandidate : secondCandidate);

                sets.Remove(firstCandidate);
                sets.Remove(secondCandidate);
            }

            selectedSets.AddRange(selectedSets.ToList());

            return selectedSets;
        }

        private ChromosomesSetsList Crossover(ChromosomesSetsList sets)
        {
            ChromosomesSetsList res = new ChromosomesSetsList();

            while (sets.Count != 0)
            {
                int firstParentIndex = _random.Next(0, sets.Count);
                int secondParentIndex = _random.Next(0, sets.Count);

                while (firstParentIndex == secondParentIndex)
                {
                    secondParentIndex = _random.Next(0, sets.Count);
                }

                ChromosomesSet firstParent = sets[firstParentIndex];
                ChromosomesSet secondParent = sets[secondParentIndex];

                var firstChild = new ChromosomesSet();
                var secondChild = new ChromosomesSet();

                for (int i = 0; i < firstParent.Count; i++)
                {
                    bool addNextGeneToFirstChild = (_random.NextDouble() < 0.5);
                    firstChild.Add((addNextGeneToFirstChild) ? firstParent[i] : secondParent[i]);
                    secondChild.Add((addNextGeneToFirstChild) ? secondParent[i] : firstParent[i]);
                }

                res.Add(firstChild);
                res.Add(secondChild);

                sets.Remove(firstParent);
                sets.Remove(secondParent);
            }

            return res;
        }

        private void Mutate(ref ChromosomesSetsList sets, IEnumerable<BankAccount> bankAccounts, DateTime dateStart, DateTime dateEnd)
        {
            for (int i = 0; i < sets.Count / 20; i++)
            {
                int randomSetIndex = _random.Next(0, sets.Count);
                int randomGeneIndex = _random.Next(0, sets[randomSetIndex].Count);

                sets[randomSetIndex][randomGeneIndex] = _generator.GenerateOnePaymentApplicationVariant(sets[randomSetIndex][randomGeneIndex],
                                                                                                        bankAccounts,
                                                                                                        dateStart, dateEnd);
            }
        }
    }
}
