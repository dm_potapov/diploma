﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTreasury.Models.SourceData
{
    /// <summary>
    /// Счёт организации в банке
    /// </summary>
    public class BankAccount : IEntity
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Номер расчётного счёта
        /// </summary>
        public string AccountNumber { get; set; }

        /// <summary>
        /// Название банка
        /// </summary>
        public string BankName { get; set; }

        /// <summary>
        /// Банковский идентификационный код
        /// </summary>
        public string BIK { get; set; }

        /// <summary>
        /// Идентификатор валюты счёта
        /// </summary>
        public Guid CurrencyId { get; set; }

        /// <summary>
        /// Остатки на счетё
        /// </summary>
        public IEnumerable<BankAccountResidue> Residues { get; set; }
    }
}
