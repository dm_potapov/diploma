﻿using SmartTreasury.Models;
using SmartTreasury.Models.SourceData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartTreasury.Models.SourceData
{
    /// <summary>
    /// Модель, содержащая в себе все исходные данные для расчёта календаря
    /// </summary>
    public class SourceDataModel
    {
        /// <summary>
        /// Подразделения предприятия
        /// </summary>
        public IEnumerable<Organization> Organizations { get; set; }

        /// <summary>
        /// Банковские счета 
        /// </summary>
        public IEnumerable<BankAccount> BankAccounts { get; set; }

        /// <summary>
        /// Заявки на платежи
        /// </summary>
        public IEnumerable<PaymentApplication> Applications { get; set; }

        /// <summary>
        /// Денежные поступления
        /// </summary>
        public IEnumerable<ReceivedPayment> Receives { get; set; }

        /// <summary>
        /// Доступные кредитные линии
        /// </summary>
        public IEnumerable<CreditLine> CreditLines { get; set; }

        /// <summary>
        /// Доступные депозитные счета
        /// </summary>
        public IEnumerable<DepositBill> DepositBills { get; set; }
    }
}