﻿namespace SmartTreasury.Models
{
    /// <summary>
    /// Тип операции заявки на платёж
    /// </summary>
    public enum OperationType
    {
        /// <summary>
        /// Оплата поставщику
        /// </summary>
        PaymentToProvider = 0,
        /// <summary>
        /// Возврат покупателю
        /// </summary>
        ReturnToBuyer = 1,
        /// <summary>
        /// Перечисление налога
        /// </summary>
        Tax = 2,
        /// <summary>
        /// Расчёты по кредитам и займам
        /// </summary>
        CreditPayment = 3,
        /// <summary>
        /// Возврат займа
        /// </summary>
        BorrowingReturn = 4,
        /// <summary>
        /// Возврат кредита
        /// </summary>
        CreditReturn = 5,
        /// <summary>
        /// Выдача займа контрагенту
        /// </summary>
        BorrowingToContractor = 6,
        /// <summary>
        /// Прочие расчёты с контрагентами
        /// </summary>
        OtherContractorsPayments = 7,
        /// <summary>
        /// Перевод на другой счёт
        /// </summary>
        TransferToOtherAccount = 8,
        /// <summary>
        /// Выдача наличных
        /// </summary>
        CashDelivery = 9,
        /// <summary>
        /// Перечисление подотчётному лицу
        /// </summary>
        AccountablePersonPayment = 10,
        /// <summary>
        /// Перечисление заработной платы
        /// </summary>
        Salary = 11,
        /// <summary>
        /// Перечисление заработной платы работнику
        /// </summary>
        SalaryToWorker = 12,
        /// <summary>
        /// Перечисление сотруднику по договору пордряда
        /// </summary>
        SalaryByWorkAgreement = 13,
        /// <summary>
        /// Перечисление депонентов
        /// </summary>
        DeponentsPayment = 14,
        /// <summary>
        /// Выдача займа работнику
        /// </summary>
        BorrowingToWorker = 15,
        /// <summary>
        /// Личные средства предпринимателя
        /// </summary>
        PrivateBusinessmanCash = 16,
        /// <summary>
        /// Прочее списание
        /// </summary>
        Other = 17,
        /// <summary>
        /// Комиссия банка
        /// </summary>
        BankCommission = 18
    }
}