﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTreasury.Models
{
    /// <summary>
    /// Депозитный счёт
    /// </summary>
    public class DepositBill : IEntity
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Дата начала доступности данного депозитного счёта
        /// </summary>
        public DateTime AvailabilityStartDate { get; set; }

        /// <summary>
        /// Дата окончания доступности данного депозитного счёта
        /// </summary>
        public DateTime AvailabilityEndDate { get; set; }

        /// <summary>
        /// Минимальная длительность депозита в днях
        /// </summary>
        public int MinDuration { get; set; }

        /// <summary>
        /// Максимальная длительность депозита в днях
        /// </summary>
        public int MaxDuration { get; set; }

        /// <summary>
        /// Минимальная сумма
        /// </summary>
        public decimal MinSum { get; set; }

        /// <summary>
        /// Максимальная сумма
        /// </summary>
        public decimal MaxSum { get; set; }

        /// <summary>
        /// Процентная ставка
        /// </summary>
        public double Percent { get; set; }

        /// <summary>
        /// Период выплаты процентов
        /// </summary>
        public PaymentPeriod PercentPaymentPeriod { get; set; }

        /// <summary>
        /// Уже выданное количество средств
        /// </summary>
        public decimal AlreadyTakenSum { get; set; }
    }
}
