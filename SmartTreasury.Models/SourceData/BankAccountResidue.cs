﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTreasury.Models.SourceData
{
    /// <summary>
    /// Остаток по расчётному счёту
    /// </summary>
    public class BankAccountResidue : IEntity
    {
        public Guid Id { get; set; }

        /// <summary>
        /// Id валюты
        /// </summary>
        public Guid CurrencyId { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Сумма на начало дня
        /// </summary>
        public decimal SumStart { get; set; }

        /// <summary>
        /// Сумма на конец дня
        /// </summary>
        public decimal SumEnd { get; set; }
    }
}
