﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTreasury.Models
{
    public class GivenDeposit
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Id организации
        /// </summary>
        public Guid OrganizationId { get; set; }

        /// <summary>
        /// Id валюты
        /// </summary>
        public Guid CurrencyId { get; set; }

        /// <summary>
        /// Id банковского счёта
        /// </summary>
        public Guid BankAccoundId { get; set; }

        /// <summary>
        /// Id депозитного счёта
        /// </summary>
        public Guid DepositBillId { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        public decimal Sum { get; set; }

        /// <summary>
        /// Дата открытия 
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Дата закрытия
        /// </summary>
        public DateTime EndDate { get; set; }
    }
}
