﻿namespace Models.Calendar
{
    public enum CreditCalculateStatus
    {
        CantGetCredit = 0,
        CreditIsAvailable = 1
    }
}