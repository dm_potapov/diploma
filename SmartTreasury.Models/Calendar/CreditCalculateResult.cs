﻿using SmartTreasury.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models.Calendar
{
    public class CreditCalculateResult
    {
        public CreditCalculateStatus Status { get; set; }

        public TakenCredit CreditInfo { get; set; }

        public IEnumerable<PaymentApplication> CreditParts { get; set; }
    }
}
