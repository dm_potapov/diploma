﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartTreasury.Models
{
    public enum PaymentSource
    {
        RandomGenerated = 0,
        Credit = 1
    }
}
