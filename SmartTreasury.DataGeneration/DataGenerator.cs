﻿using SmartTreasury.Models;
using SmartTreasury.Models.SourceData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTreasury.DataGeneration
{
    public class DataGenerator
    {
        private readonly Random _random;

        public DataGenerator()
        {
            _random = new Random();
        }

        public IEnumerable<PaymentApplication> GenerateApplications(DateTime startDate, DateTime endDate, 
                                                                    int count,
                                                                    decimal minSum, decimal maxSum)
        {
            List<PaymentApplication> apps = new List<PaymentApplication>();
            foreach (var i in Enumerable.Range(0, count))
            {
                var dateCreated = GetRandomDateInRange(startDate, endDate.AddDays(-1));
                apps.Add(new PaymentApplication()
                {
                    StartDate = dateCreated,
                    DeadlineDate = GetRandomDateInRange(dateCreated, endDate),
                    TreasureDate = null,
                    Sum = GetRandomSum(minSum, maxSum),
                    Source = PaymentSource.RandomGenerated
                });
            }

            return apps;
        }

        public IEnumerable<ReceivedPayment> GenerateReceives(List<BankAccount> accounts,
                                                                DateTime startDate, DateTime endDate, 
                                                                int count,
                                                                decimal minSum, decimal maxSum)
        {

            var receives = new List<ReceivedPayment>();
            foreach (var i in Enumerable.Range(0, count))
            {
                receives.Add(new ReceivedPayment()
                {
                    BankAccountId = accounts[_random.Next(0, accounts.Count)].Id,
                    ReceiveDate = GetRandomDateInRange(startDate, endDate),
                    Sum = GetRandomSum(minSum, maxSum),
                    Source = PaymentSource.RandomGenerated
                });
            }

            return receives;
        }

        public IEnumerable<BankAccount> GenerateBankAccounts(int count)
        {
            List<BankAccount> accs = new List<BankAccount>();

            foreach (int i in Enumerable.Range(0, count))
            {
                accs.Add(new BankAccount()
                {
                    Id = Guid.NewGuid(),
                    AccountNumber = _random.Next(100000000, 999999999).ToString(),
                    BankName = "Test bank name",
                    Residues = new List<BankAccountResidue>()
                });
            }

            return accs;
        }

        public SourceDataModel GenerateSourceDataModel(DataGenerationParameters parameters)
        {
            SourceDataModel res = new SourceDataModel();

            res.BankAccounts = GenerateBankAccounts(parameters.AccountsCount);
            res.Applications = GenerateApplications(parameters.DateStart, parameters.DateEnd,
                                                    parameters.AppsCount, 10000, 100000);
            res.Receives = GenerateReceives(res.BankAccounts.ToList(), parameters.DateStart, parameters.DateEnd,
                                                    parameters.ReceivesCount, 10000, 100000);
            res.CreditLines = new List<CreditLine>()
            {
                new CreditLine()
                {
                    AvailabilityStartDate = DateTime.SpecifyKind(DateTime.Parse("2017-01-01"), DateTimeKind.Utc),
                    AvailabilityEndDate = DateTime.SpecifyKind(DateTime.Parse("2017-12-31"), DateTimeKind.Utc),
                    MinDuration = 30,
                    MaxDuration = 366,
                    MinSum = 10000,
                    MaxSum = 1000000,
                    AlreadyGivenSum = 0,
                    IsRenewable = true,
                    Percent = 13,
                    PercentPaymentPeriod = PaymentPeriod.Year
                }
            };

            res.DepositBills = new List<DepositBill>()
            {
                new DepositBill()
                {
                    AvailabilityStartDate = DateTime.SpecifyKind(DateTime.Parse("2017-01-01"), DateTimeKind.Utc),
                    AvailabilityEndDate = DateTime.SpecifyKind(DateTime.Parse("2017-12-31"), DateTimeKind.Utc),
                    MinDuration = 30,
                    MaxDuration = 366,
                    MinSum = 10000,
                    MaxSum = 1000000,
                    Percent = 13,
                    PercentPaymentPeriod = PaymentPeriod.Year
                }
            };

            return res;
        }

        private decimal GetRandomSum(decimal minSum, decimal maxSum)
        {
            return _random.Next((int)minSum, (int)maxSum);
        }

        private DateTime GetRandomDateInRange(DateTime start, DateTime end)
        {
            int timestampStart = (int)(new DateTimeOffset(start).ToUnixTimeSeconds());
            int timestampEnd = (int)(new DateTimeOffset(end).ToUnixTimeSeconds());

            int randomTimestamp = _random.Next(timestampStart, timestampEnd);

            var generatedDate = (new DateTimeOffset(1970, 01, 01, 0, 0, 0, new TimeSpan())).AddSeconds(randomTimestamp).Date;

            return DateTime.SpecifyKind(generatedDate, DateTimeKind.Utc);
        }
    }
}
