﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTreasury.DataGeneration
{
    public class DataGenerationParameters
    {
        public int AppsCount { get; set; }
        public int ReceivesCount { get; set; }
        public int AccountsCount { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
    }
}
