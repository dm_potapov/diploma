﻿using MongoDB.Driver;
using Newtonsoft.Json;
using SmartTreasury.Models;
using SmartTreasury.Models.SourceData;
using SmartTreasury.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTreasury.DataGeneration
{
    class Program
    {
        static void Main(string[] args)
        {
            //// Инициализация
            //DateTime startDate = DateTime.Parse("2017-01-01");
            //DateTime endDate = DateTime.Parse("2017-12-31");
            //int applicationsCount = 100;
            //int receivesCount = 10;
            //decimal minAppSum = 1000;
            //decimal maxAppSum = 10000;
            //decimal minReceiveSum = 10000;
            //decimal maxReceiveSum = 200000;

            //DataGenerator dataGenerator = new DataGenerator();

            //Guid bankAccountId = Guid.NewGuid();

            //// Генерация
            //var applications = dataGenerator.GenerateApplications(bankAccountId, startDate, endDate,
            //                                                     applicationsCount,
            //                                                     minAppSum, maxAppSum);
            //var receives = dataGenerator.GenerateReceives(bankAccountId, startDate, endDate,
            //                                              receivesCount,
            //                                              minReceiveSum, maxReceiveSum);
            //var creditLines = new List<CreditLine>()
            //{
            //    new CreditLine()
            //    {
            //        AvailabilityStartDate = DateTime.Parse("2017-01-01"),
            //        AvailabilityEndDate = DateTime.Parse("2017-12-31"),
            //        MinDuration = 30,
            //        MaxDuration = 366,
            //        MinSum = 10000,
            //        MaxSum = 1000000,
            //        AlreadyGivenSum = 0,
            //        IsRenewable = true,
            //        Percent = 13,
            //        PercentPaymentPeriod = PaymentPeriod.Year
            //    }
            //};

            //var bankAccounts = new List<BankAccount>()
            //{
            //    new BankAccount()
            //    {
            //        Id = bankAccountId,
            //        AccountNumber = "12345",
            //        BankName = "TestBank",
            //        BIK = "BIKTest",
            //        Residues = new List<BankAccountResidue>()
            //    }
            //};

            //// Сохранение в json-документы
            //SaveIEnumerableToJson(applications, "test_apps.json");
            //SaveIEnumerableToJson(receives, "test_receives.json");
            //SaveIEnumerableToJson(creditLines, "test_credit_lines.json");
            //SaveIEnumerableToJson(bankAccounts, "test_bank_accounts.json");
        }

        static void SaveIEnumerableToMongo<T>(IEnumerable<T> enumerable) where T : IEntity
        {
            var mongoDbRepository = new MongoDbRepository<T>(new MongoClient("mongodb://localhost:27017"),
                                                          "smartTreasury");
            foreach (T item in enumerable)
            {
                mongoDbRepository.Save(item);
            }
        }

        static void SaveIEnumerableToJson<T>(IEnumerable<T> enumerable, string path)
        {
            File.WriteAllText(path, JsonConvert.SerializeObject(enumerable), Encoding.UTF8);
        }
    }
}
