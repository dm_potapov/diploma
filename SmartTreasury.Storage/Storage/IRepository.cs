﻿using SmartTreasury.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartTreasury.Storage
{
    /// <summary>
    /// Интерфейс для репозитория
    /// </summary>
    /// <typeparam name="TEntity">Объект, реализующий интерфейс IEntity</typeparam>
    public interface IRepository<TEntity> : IQueryable<TEntity> where TEntity : IEntity
    {
        /// <summary>
        /// Возвращает элемент из репозитория по его ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TEntity GetById(Guid id);

        /// <summary>
        /// Обновляет элемент, либо добавляет новый, если искомого элемента нет
        /// </summary>
        /// <param name="T"></param>
        void Save(TEntity T);

        /// <summary>
        /// Удаляет элемент
        /// </summary>
        /// <param name="T"></param>
        void Delete(TEntity T);
    }
}
