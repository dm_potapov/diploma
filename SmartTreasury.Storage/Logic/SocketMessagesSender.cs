﻿using SmartTreasury.Storage.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SmartTreasury.Shared.Logic
{
    public class SocketMessagesSender
    {
        private const int _readBufferSize = 1024;

        public void Send(string message, int port)
        {
            IPHostEntry ipHost = Dns.GetHostEntry("localhost");
            IPAddress ip = ipHost.AddressList[0];
            IPEndPoint endPoint = new IPEndPoint(ip, port);

            using (Socket sender = new Socket(ip.AddressFamily, SocketType.Stream, ProtocolType.Tcp))
            {
                sender.Connect(endPoint);

                byte[] msgBytes = Encoding.UTF8.GetBytes(message);
                sender.Send(msgBytes);
                sender.Shutdown(SocketShutdown.Send);
                sender.Close();
            }
           
        }

        public string WaitForMessage(int portListen)
        {
            IPHostEntry ipHost = Dns.GetHostEntry("localhost");
            IPAddress ip = ipHost.AddressList[0];
            IPEndPoint endPoint = new IPEndPoint(ip, portListen);

            using (Socket listener = new Socket(ip.AddressFamily, SocketType.Stream, ProtocolType.Tcp))
            {
                listener.Bind(endPoint);
                listener.Listen(10);

                Socket handler = listener.Accept();

                //SocketsAsyncStateObject so = new SocketsAsyncStateObject()
                //{
                //    workSocket = listener
                //};

                //listener.BeginAccept(Listen_Callback, so);

                //string data = so.sb.ToString();

                int bytesTotal = handler.Available;

                string data = "";

                for (int i = 0; i < bytesTotal; i += _readBufferSize)
                {
                    byte[] bytes = new byte[_readBufferSize];
                    int bytesRec = handler.Receive(bytes);

                    data += Encoding.UTF8.GetString(bytes, 0, bytesRec);
                }

                handler.Shutdown(SocketShutdown.Both);
                handler.Close();

                return data;
            }
        }

        public string SendAndWaitForAnswer(string message, int portSend, int portListen)
        {
            Send(message, portSend);

            return WaitForMessage(portListen);
        }

        public static void Listen_Callback(IAsyncResult ar)
        {
            SocketsAsyncStateObject so = (SocketsAsyncStateObject)ar.AsyncState;
            Socket s2 = so.workSocket.EndAccept(ar);
            so.workSocket = s2;
            s2.BeginReceive(so.buffer, 0, SocketsAsyncStateObject.BufferSize, 0,
                                  new AsyncCallback(Read_Callback), so);
        }

        public static void Read_Callback(IAsyncResult ar)
        {
            SocketsAsyncStateObject so = (SocketsAsyncStateObject)ar.AsyncState;
            Socket s = so.workSocket;

            int read = s.EndReceive(ar);

            if (read > 0)
            {
                so.sb.Append(Encoding.UTF8.GetString(so.buffer, 0, read));
                s.BeginReceive(so.buffer, 0, SocketsAsyncStateObject.BufferSize, 0,
                                         new AsyncCallback(Read_Callback), so);
            }
            else
            {
                if (so.sb.Length > 1)
                {
                    //All of the data has been read, so displays it to the console
                    string strContent;
                    strContent = so.sb.ToString();
                    Console.WriteLine(string.Format("Read {0} byte from socket" +
                                       "data = {1} ", strContent.Length, strContent));
                }
                s.Shutdown(SocketShutdown.Both);
                s.Close();
            }
        }
    }
}