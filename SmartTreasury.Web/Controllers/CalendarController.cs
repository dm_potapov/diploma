﻿namespace SmartTreasury.Web.Controllers
{
    using Classes.Models.Api;
    using Main;
    using Models;
    using Models.SourceData;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Shared.Logic;
    using Storage;
    using Storage.Logic;
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web.Http;

    [RoutePrefix("api/calendar")]
    public class CalendarController : ApiController
    {
        private readonly IRepository<PaymentApplication> _applications;
        private readonly IRepository<ReceivedPayment> _receives;
        private readonly IRepository<CreditLine> _creditLines;
        private readonly Dictionary<Guid, Task<Guid>> _algoInstances;
        private readonly Dictionary<Guid, AlgorithmInstanceState> _algoStates;
        private readonly DataSaver _dataSaver;
        private readonly PaymentsCalendarMakingProcess _process;
        private readonly IRepository<PaymentsCalendarCalculationResult> _results;
        private readonly IRepository<BankAccount> _accounts;

        public CalendarController(IRepository<PaymentApplication> _applications,
                                  IRepository<ReceivedPayment> _receives,
                                  IRepository<CreditLine> _creditLines,
                                  Dictionary<Guid, Task<Guid>> _algoInstances,
                                  Dictionary<Guid, AlgorithmInstanceState> _algoStates,
                                  DataSaver _dataSaver, PaymentsCalendarMakingProcess _process, IRepository<PaymentsCalendarCalculationResult> _results, IRepository<BankAccount> _accounts)
        {
            if (_applications == null) throw new ArgumentNullException();
            if (_receives == null) throw new ArgumentNullException();
            if (_creditLines == null) throw new ArgumentNullException();
            if (_algoInstances == null) throw new ArgumentNullException();
            if (_algoStates == null) throw new ArgumentNullException();
            if (_dataSaver == null) throw new ArgumentNullException();
            if (_process == null) throw new ArgumentNullException();

            this._applications = _applications;
            this._receives = _receives;
            this._creditLines = _creditLines;
            this._algoInstances = _algoInstances;
            this._dataSaver = _dataSaver;
            this._algoStates = _algoStates;
            this._process = _process;
            this._results = _results;
            this._accounts = _accounts;
        }

        [HttpPut]
        [Route("put")]
        public IHttpActionResult PutData(SourceDataModel model)
        {
            _dataSaver.DropAndSaveFromSourceDataModel(model);
            return Ok();
        }

        [HttpPost]
        [Route("computing/start")]
        public IHttpActionResult StartComputing(ComputingStartDTO dto)
        {
            var newSessionGuid = Guid.NewGuid();
            Task<Guid> newAlgo = new Task<Guid>
                                                (() => _process.DoProcess(dto.DateStart, dto.DateEnd, dto.IterationsCount, dto.PopulationSize, newSessionGuid));
            _algoInstances.Add(newSessionGuid, newAlgo);
            _algoStates.Add(newSessionGuid, new AlgorithmInstanceState
            {
                CancellationToken = false,
                Counter = 0
            });
            newAlgo.Start();
            return Ok(new { SessionId = newSessionGuid });
        }

        [HttpPost]
        [Route("computing/stop")]
        public IHttpActionResult StopComputing(ComputingCheckDTO dto)
        {
            _algoStates[dto.SessionId].CancellationToken = true;

            return Ok();
        }

        [HttpPost]
        [Route("computing/status")]
        public IHttpActionResult CheckComputingStatus(ComputingCheckDTO dto)
        {
            try
            {
                bool isAlgorithmCompleted = _algoInstances[dto.SessionId].IsCompleted;
                var answer = new
                {
                    IsCompleted = isAlgorithmCompleted,
                    IterationNumber = _algoStates[dto.SessionId].Counter,
                    ResultId = (isAlgorithmCompleted) ? _algoInstances[dto.SessionId].Result
                                                    : Guid.Empty
                };
                return Ok(answer);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        [Route("result/frontend")]
        public IHttpActionResult GetResultForFrontendById(CalendarViewParametersDTO dto)
        {
            try
            {
                PaymentsCalendarCalculationResult res = _results.GetById(dto.CalendarResultId);

                return Ok(new PaymentsCalendar(_accounts, res.Applications, res.Receives, dto.DateStart, dto.DateEnd));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        [Route("results/guids")]
        public IHttpActionResult GetResultsGuids()
        {
            return Ok(_results.Select(r => r.Id).ToList());
        }
    }
}
