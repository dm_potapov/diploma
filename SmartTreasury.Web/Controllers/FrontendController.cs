﻿using SmartTreasury.DataGeneration;
using SmartTreasury.Models;
using SmartTreasury.Models.SourceData;
using SmartTreasury.Storage;
using SmartTreasury.Storage.Logic;
using SmartTreasury.Web.Classes.Models.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SmartTreasury.Web.Controllers
{
    [RoutePrefix("api/frontend")]
    public class FrontendController : ApiController
    {
        private readonly IRepository<PaymentApplication> _applications;
        private readonly IRepository<ReceivedPayment> _receives;
        private readonly IRepository<CreditLine> _creditLines;
        private readonly IRepository<DepositBill> _depositBills;
        private readonly IRepository<BankAccount> _bankAccounts;
        private readonly DataSaver _dataSaver;
        private readonly DataGenerator _generator;

        public FrontendController(IRepository<PaymentApplication> _applications, IRepository<ReceivedPayment> _receives, IRepository<CreditLine> _creditLines, IRepository<DepositBill> _depositBills, IRepository<BankAccount> _bankAccounts, DataSaver _dataSaver, DataGenerator _generator)
        {
            this._applications = _applications;
            this._receives = _receives;
            this._creditLines = _creditLines;
            this._depositBills = _depositBills;
            this._bankAccounts = _bankAccounts;
            this._dataSaver = _dataSaver;
            this._generator = _generator;
        }

        [HttpPost]
        [Route("generate")]
        public void GenerateData(DataGenerationParameters parameters)
        {
            SourceDataModel model = _generator.GenerateSourceDataModel(parameters);
            _dataSaver.DropAndSaveFromSourceDataModel(model);
        }

        [HttpPost]
        [Route("accounts")]
        public object GetAccounts()
        {
            return _bankAccounts.ToList();
        }

        [HttpPost]
        [Route("applications")]
        public object GetApplications()
        {
            return _applications.ToList();
        }

        [HttpPost]
        [Route("receives")]
        public object GetReceives()
        {
            return _receives.ToList();
        }
    }
}
