﻿angular.module('smartTreasury')
.component('calendar', {
    bindings: {
        results: '='
    },
    templateUrl: "templates/calendar.html",
    controller: function ($http, AlertService, $state) {
        var $ctrl = this;
        $ctrl.isWorkSuspended = false;
        $ctrl.formData = {
            StartDate: "01-07-2017",
            EndDate: "01-07-2018",
            IterationsCount: 100,
            PopulationSize: 20
        };
        $ctrl.startMakingCalendar = function () {
            $http.post('/api/calendar/computing/start', $ctrl.formData)
                .then(function (resp) {
                    $ctrl.percent = 0;
                    $ctrl.isCalendarMaking = true;
                    $ctrl.sessionId = resp.data.SessionId;
                    $ctrl.checkMakingStatus();
                }, window.onAjaxError);
        };
        $ctrl.suspendWork = function() {
            if ($ctrl.isCalendarMaking) {
                $http.post('/api/calendar/computing/stop', { SessionId: $ctrl.sessionId })
                .then(function (resp) {
                    $ctrl.isWorkSuspended = true;
                }, window.onAjaxError);
            }
        },
        $ctrl.checkMakingStatus = function () {
            if ($ctrl.isCalendarMaking) {
                $http.post('/api/calendar/computing/status', { SessionId: $ctrl.sessionId })
                .then(function (resp) {
                    $ctrl.percent = Math.round(resp.data.IterationNumber * (100 / $ctrl.formData.IterationsCount));
                    if (resp.data.IsCompleted) {
                        $ctrl.isCalendarMaking = false;
                        $ctrl.isWorkSuspended = false;
                        AlertService.showMessage('success', 'Календарь успешно составлен', 3000);
                        $ctrl.formData = {};
                        $state.reload();
                    }
                    else {
                        setTimeout($ctrl.checkMakingStatus, 1000);
                    }
                }, window.onAjaxError);
            }
        }
        $ctrl.isCalendarMaking = false;
    }
})
.component('calendarinfo', {
    bindings: {
        resultId: '='
    },
    templateUrl: "templates/calendar/info.html",
    controller: function ($state, $scope, $http, AlertService, DateTransformService) {
        var $ctrl = this;
        $ctrl.formData = {
            CalendarResultId: window.resultId
        };
        $ctrl.dates = []
        $ctrl.calendarDetails = {};
        $ctrl.onPeriodChoose = function () {
            $http.post('/api/calendar/result/frontend', $ctrl.formData)
                .then(function (resp) {
                    $ctrl.dates = $ctrl.makeDates($ctrl.formData.DateStart, $ctrl.formData.DateEnd);
                    $ctrl.calendarDetails = resp.data.Residues;
                }, window.onAjaxError);
        }
        $ctrl.makeDates = function (dateStart, dateEnd) {
            var res = [];
            var timestamp = dateStart.getTime();
            for (; timestamp <= dateEnd.getTime() ; timestamp += 86400000) {
                res.push(DateTransformService.transformDate(new Date(timestamp).toString(), "y-m-d"));
            }
            console.log(res);
            return res;
        },
        $ctrl.formatAsSum = function(num) {
            var numAsStr = num.toString();
            var isNeg = false;
            if (numAsStr[0] == "-") {
                numAsStr = numAsStr.substr(1);
                isNeg = true
            }
            var formattedNumber = "";
            var i = numAsStr.length - 3;
            for (; i >= 1; i -= 3) {
                formattedNumber = "," + numAsStr.substr(i, 3) + formattedNumber;
            }
            formattedNumber = numAsStr.substr(0, i + 3) + formattedNumber;
            if (isNeg) {
                formattedNumber = "-" + formattedNumber;
            }
            return formattedNumber;
        }
    }
})