﻿angular.module('smartTreasury')
.component('viewdata', {
    bindings: {
        receives: "=",
        accounts: "="
    },
    templateUrl: "templates/data.html",
    controller: function ($http, $state) {
        var $ctrl = this;
        $ctrl.formData = {};
        $ctrl.generateData = function () {
            $http.post('/api/frontend/generate', $ctrl.formData)
                .then(function (resp) {
                    alert("Данные успешно сгенерированы");
                    $state.reload();
                }, window.onAjaxError);
        }
    }
})