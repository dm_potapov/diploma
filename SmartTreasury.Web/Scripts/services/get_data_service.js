﻿angular.module('smartTreasury')
.service('GetDataService', function ($state, $http, 
                                    DateTransformService, AlertService) {
    return {
        GetApplications: function () {
            return $http.post('api/frontend/applications')
            .then(function (resp) {
                DateTransformService.transformMultipleDates(resp.data, "StartDate", "d-m-y");
                DateTransformService.transformMultipleDates(resp.data, "DeadlineDate", "d-m-y");
                console.log("got apps");
                return resp.data;
            }, window.onAjaxError);
        },
        GetReceives: function () {
            return $http.post('api/frontend/receives')
            .then(function (resp) {
                DateTransformService.transformMultipleDates(resp.data, "ReceiveDate", "d-m-y");
                return resp.data;
            }, window.onAjaxError);
        },
        GetAccounts: function () {
            return $http.post('api/frontend/accounts')
            .then(function (resp) {
                return resp.data;
            }, window.onAjaxError);
        },
        GetResults: function () {
            return $http.post('api/calendar/results/guids')
            .then(function (resp) {
                return resp.data;
            }, window.onAjaxError);
        }
    }
});