﻿angular.module('smartTreasury')
.service('DateTransformService', function () {

    function getTwoSignsDate(date) {
        return ((date.toString()).length == 1)
                ? "0" + date
                : "" + date;
    }

    function transformDate(dateIn, format) {
        var date = new Date(dateIn);
        format = format.replace("y", date.getFullYear())
                       .replace("m", getTwoSignsDate(date.getMonth() + 1))
                       .replace("d", getTwoSignsDate(date.getDate()));
        return format;
    }

    return {
        transformDate: transformDate,
        transformMultipleDates: function(collection, dateFieldName, format) {
            for (i in collection) {
                collection[i][dateFieldName] = transformDate(collection[i][dateFieldName], format);
            }
            return collection;
        }
    }

});