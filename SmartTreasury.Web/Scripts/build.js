﻿var angular = require('angular');
//require('ui-router-core');
require('angular-ui-router');
require('angular-animate');
require('angular-sanitize');
require('angular-ui-bootstrap');

require('./main');

require('./services/get_data_service');
require('./services/date_transform_service');
require('./services/alert_service');

require('./components/main');
require('./components/viewdata');
require('./components/calendar');
require('./components/data/applications');
require('./components/data/accounts');
require('./components/data/receives');