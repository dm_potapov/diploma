﻿window.onAjaxError = function (response) {
    alert(response.data);
}

var app = angular.module("smartTreasury", ['ui.router', 'ngAnimate', 'ui.bootstrap']);

app.config(function ($stateProvider) {
    $stateProvider
        .state({
            name: 'main',
            url: '/',
            component: 'main'
        })
        .state({
            name: 'viewdata',
            url: '/data',
            component: 'viewdata',
            resolve: {
                receives: function(GetDataService) {
                    return GetDataService.GetReceives();
                },
                accounts: function (GetDataService) {
                    return GetDataService.GetAccounts();
                }
            }
        })
      .state({
          name: 'viewdata.applications',
          url: '/applications',
          component: "applications",
          resolve: {
              applications: function (GetDataService) {
                  return GetDataService.GetApplications();
              }
          }
      })
      .state({
          name: 'viewdata.receives',
          url: '/receives',
          component: "receives",
          resolve: {
              receives: function (GetDataService) {
                  return GetDataService.GetReceives();
              }
          }
      })
        .state({
            name: 'viewdata.accounts',
            url: '/accounts',
            component: "accounts",
            resolve: {
                accounts: function (GetDataService) {
                    return GetDataService.GetAccounts();
                }
            }
        })
      .state({
          name: 'calendar',
          url: '/calendar',
          component: 'calendar',
          resolve: {
              results: function (GetDataService) {
                  return GetDataService.GetResults();
              }
          }
      })
      .state({
          name: 'calendarinfo',
          url: '/calendar/{id:string}',
          component: 'calendarinfo',
          resolve: {
              resultId: function ($transition$) {
                  window.resultId = $transition$.params().id;
                  return $transition$.params().id;
              }
          }
      });
});