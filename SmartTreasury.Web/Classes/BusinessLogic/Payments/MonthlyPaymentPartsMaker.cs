﻿using SmartTreasury.Classes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartTreasury.Classes.BusinessLogic.Payments
{
    /// <summary>
    /// Генератор ежемесячных платежей
    /// </summary>
    public class MonthlyPaymentPartsMaker
    {
        /// <summary>
        /// Генерирует список ежемесячных платежей и возвращает его
        /// </summary>
        /// <param name="createDate">Начало срока</param>
        /// <param name="endDate">Окончание срока</param>
        /// <param name="sum">Общая сумма платежей</param>
        /// <returns></returns>
        public IEnumerable<PaymentPart> GetPaymentParts(DateTime createDate, DateTime endDate, decimal sum)
        {
            int paymentPartsCount = getPaymentPartsCount(createDate, endDate);
            decimal sumPart = Math.Round(sum / paymentPartsCount, 2, MidpointRounding.AwayFromZero);
            decimal sumRemain = sum - (sumPart * paymentPartsCount);

            List<PaymentPart> paymentParts = new List<PaymentPart>();
            
            DateTime nextDate = endDate;
            foreach (int i in Enumerable.Range(0, paymentPartsCount)) {
                paymentParts.Add(new PaymentPart()
                {
                    Date = nextDate,
                    Sum = sumPart
                });
                nextDate = nextDate.AddMonths(-1);
            }

            paymentParts.Last().Sum += sumRemain;

            return paymentParts;
        } 

        /// <summary>
        /// Возвращает количество платежей
        /// </summary>
        /// <param name="createDate">Дата начала</param>
        /// <param name="endDate">Дата окончания</param>
        /// <returns></returns>
        private int getPaymentPartsCount(DateTime createDate, DateTime endDate)
        {
            if (Math.Abs(createDate.DayOfYear - endDate.DayOfYear) % 365 < 31)
            {
                return 1;
            }
            else
            {
                return Math.Abs(createDate.Month - endDate.Month) % 12 + 1;
            }
        }
    }
}
