﻿using SmartTreasury.Classes.Models;
using SmartTreasury.Classes.Models.Schedules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartTreasury.Classes.BusinessLogic.Schedules
{
    public class ScheduleItemDTOCollectionFactory
    {
        public IEnumerable<ScheduleItemDTO> FromScheduleItemsListAndPaymentsList
                                                (IEnumerable<ScheduleItem> scheduleItems,
                                                IEnumerable<Payment> payments)
        {
            IEnumerable<ScheduleItemDTO> res = scheduleItems.Join
                                                (payments,
                                                s => s.PaymentId,
                                                p => p.Id,
                                                (sv, pv) => new ScheduleItemDTO() {
                                                    Date = sv.Date,
                                                    PaymentId = sv.PaymentId,
                                                    PaymentName = pv.Name,
                                                    Sum = sv.Sum
                                                });

            return res;

        }
    }
}
