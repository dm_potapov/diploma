﻿using SmartTreasury.Classes.BusinessLogic.Payments;
using SmartTreasury.Classes.BusinessLogic.Schedules;
using SmartTreasury.Classes.Models;
using SmartTreasury.Classes.Models.Schedules;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SmartTreasury.Classes.BusinessLogic
{
    /// <summary>
    /// Класс, отвечающий за формирование графика платежей
    /// </summary>
    public class ScheduleFormer : IScheduleFormer
    {
        /// <summary>
        /// Класс-прослойка для доступа к хранилищу платёжных обязательств
        /// </summary>
        private PaymentsService _service;

        /// <summary>
        /// Генератор ежемесячных платежей для платёжного обязательства
        /// </summary>
        private MonthlyPaymentPartsMaker _maker;

        /// <summary>
        /// Фабрика объектов класса ScheduleItem
        /// </summary>
        private ScheduleItemFactory _factory;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="service"></param>
        /// <param name="maker"></param>
        /// <param name="factory"></param>
        public ScheduleFormer(PaymentsService service, MonthlyPaymentPartsMaker maker,
                                ScheduleItemFactory factory)
        {
            _service = service;
            _maker = maker;
            _factory = factory;
        }

        /// <summary>
        /// Вставка ежемесячных платежей
        /// </summary>
        /// <param name="schedule"></param>
        /// <param name="payments"></param>
        private void insertMonthlyPayments(ref Schedule schedule, IEnumerable<Payment> payments)
        {
            foreach (Payment p in payments)
            {
                IEnumerable<PaymentPart> monthlyPaymentParts = 
                                _maker.GetPaymentParts(p.CreateDate, p.PaymentParts.Last().Date, p.Sum); 
                foreach (PaymentPart pp in monthlyPaymentParts)
                {
                    ScheduleItem item = _factory.GetFromPaymentPart(pp, p.Id);
                    schedule.ScheduleItems.Add(item);
                }
            }
        }

        /// <summary>
        /// Вставка одноразовых платежей
        /// </summary>
        /// <param name="schedule"></param>
        /// <param name="payments"></param>
        private void insertOneTimePayments(ref Schedule schedule, IEnumerable<Payment> payments)
        {
            foreach (Payment p in payments)
            {
                ScheduleItem item = _factory.GetFromPaymentPart(p.PaymentParts.Last(), p.Id);
                schedule.ScheduleItems.Add(item);
            }
        }

        /// <summary>
        /// Вставка платежей по графику
        /// </summary>
        /// <param name="schedule"></param>
        /// <param name="payments"></param>
        private void insertCustomPayments(ref Schedule schedule, IEnumerable<Payment> payments)
        {
            foreach (Payment p in payments)
            {
                foreach(PaymentPart pp in p.PaymentParts)
                {
                    ScheduleItem item = _factory.GetFromPaymentPart(pp, p.Id);
                    schedule.ScheduleItems.Add(item);
                }
            }
        }

        /// <summary>
        /// Формирует график платежей
        /// </summary>
        /// <returns></returns>
        public Schedule FormSchedule()
        {
            IEnumerable<Payment> payments = _service.Get();
            Schedule schedule = new Schedule()
            {
                Id = Guid.NewGuid(),
                CreateDate = DateTime.Now,
                ScheduleItems = new List<ScheduleItem>()
            };
            insertMonthlyPayments(ref schedule, payments.Where(v => v.PType == PaymentType.Monthly));
            insertOneTimePayments(ref schedule, payments.Where(v => v.PType == PaymentType.OneTime));
            insertCustomPayments(ref schedule, payments.Where(v => v.PType == PaymentType.Custom));
            return schedule;
        }
    }
}