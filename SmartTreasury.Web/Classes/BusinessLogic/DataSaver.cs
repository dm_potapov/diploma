﻿using Autofac;
using MongoDB.Driver;
using SmartTreasury.Models;
using SmartTreasury.Models.SourceData;
using SmartTreasury.Web.Classes.Models.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTreasury.Storage.Logic
{
    public class DataSaver
    {
        private readonly ILifetimeScope _container;

        public DataSaver(ILifetimeScope container)
        {
            if (container == null) throw new ArgumentNullException();

            _container = container;
        }

        public void DropAndSaveFromSourceDataModel(SourceDataModel model)
        {
            var client = _container.Resolve<MongoClient>();
            client.DropDatabase("smartTreasury");

            SaveIEnumerableToRepo(model.Applications);
            SaveIEnumerableToRepo(model.BankAccounts);
            SaveIEnumerableToRepo(model.CreditLines);
            SaveIEnumerableToRepo(model.DepositBills);
            SaveIEnumerableToRepo(model.Organizations);
            SaveIEnumerableToRepo(model.Receives);
        }

        private void SaveIEnumerableToRepo<T>(IEnumerable<T> enumerable) where T : IEntity
        {
            if (enumerable != null)
            {
                var repository = _container.Resolve<IRepository<T>>();
                foreach (T item in enumerable)
                {
                    if (item.Id == Guid.Empty)
                    {
                        item.Id = Guid.NewGuid();
                    }
                    repository.Save(item);
                }
            }
        }
    }
}
