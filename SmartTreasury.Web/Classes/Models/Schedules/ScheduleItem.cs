﻿using System;

namespace SmartTreasury.Classes.Models.Schedules
{
    /// <summary>
    /// Класс, представляющий часть графика платежей
    /// </summary>
    public class ScheduleItem
    {
        /// <summary>
        /// Id платёжного обязательства
        /// </summary>
        public Guid PaymentId { get; set; }

        /// <summary>
        /// Дата платежа по графику
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Сумма платежа
        /// </summary>
        public decimal Sum { get; set; }
    }
}