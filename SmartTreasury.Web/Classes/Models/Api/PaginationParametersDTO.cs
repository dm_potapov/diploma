﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartTreasury.Web.Classes.Models.Api
{
    public class PaginationParametersDTO
    {
        public int Count { get; set; }

        public int Offset { get; set; }
    }
}