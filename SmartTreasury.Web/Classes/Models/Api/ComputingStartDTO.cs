﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartTreasury.Web.Classes.Models.Api
{
    public class ComputingStartDTO
    {
        public DateTime DateStart { get; set; }

        public DateTime DateEnd { get; set; }

        public int IterationsCount { get; set; }

        public int PopulationSize { get; set; }
    }
}