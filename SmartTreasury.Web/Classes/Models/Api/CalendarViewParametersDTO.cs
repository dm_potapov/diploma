﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartTreasury.Web.Classes.Models.Api
{
    public class CalendarViewParametersDTO
    {
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public Guid CalendarResultId { get; set; }
    }
}