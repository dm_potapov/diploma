﻿using Autofac;
using MongoDB.Driver;
using SmartTreasury.Classes.BusinessLogic;
using SmartTreasury.Classes.BusinessLogic.Payments;
using SmartTreasury.Classes.BusinessLogic.Schedules;
using SmartTreasury.Classes.Models;
using SmartTreasury.Classes.Models.Schedules;
using SmartTreasury.DataGeneration;
using SmartTreasury.Main;
using SmartTreasury.Models;
using SmartTreasury.Models.SourceData;
using SmartTreasury.Shared.Logic;
using SmartTreasury.Storage;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;

namespace SmartTreasury.Config
{
    public static class AutofacConfig
    {
        public static IContainer Config()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<MongoClient>().AsSelf()
                        .WithParameter("connectionString", ConfigurationManager.AppSettings["mongoDbHost"] as string)
                        .InstancePerLifetimeScope();

            string dbName = ConfigurationManager.AppSettings["mongoDbName"] as string;

            builder.RegisterType<Dictionary<Guid, Task<Guid>>>().SingleInstance();
            builder.RegisterType<Dictionary<Guid, AlgorithmInstanceState>>().SingleInstance();
            builder.RegisterType<MongoDbRepository<Schedule>>().As<IRepository<Schedule>>()
                            .WithParameter("dbName", dbName);
            builder.RegisterType<MongoDbRepository<Payment>>().As<IRepository<Payment>>()
                            .WithParameter("dbName", dbName);
            builder.RegisterType<MongoDbRepository<PaymentApplication>>().As<IRepository<PaymentApplication>>()
                            .WithParameter("dbName", dbName);
            builder.RegisterType<MongoDbRepository<ReceivedPayment>>().As<IRepository<ReceivedPayment>>()
                            .WithParameter("dbName", dbName);
            builder.RegisterType<MongoDbRepository<CreditLine>>().As<IRepository<CreditLine>>()
                            .WithParameter("dbName", dbName);
            builder.RegisterType<MongoDbRepository<Organization>>().As<IRepository<Organization>>()
                            .WithParameter("dbName", dbName);
            builder.RegisterType<MongoDbRepository<BankAccount>>().As<IRepository<BankAccount>>()
                            .WithParameter("dbName", dbName);
            builder.RegisterType<MongoDbRepository<DepositBill>>().As<IRepository<DepositBill>>()
                            .WithParameter("dbName", dbName);
            builder.RegisterType<MongoDbRepository<PaymentsCalendarCalculationResult>>()
                            .As<IRepository<PaymentsCalendarCalculationResult>>()
                            .WithParameter("dbName", dbName);

            builder.RegisterAssemblyTypes(typeof(SocketMessagesSender).Assembly);
            builder.RegisterAssemblyTypes(typeof(GeneticAlgorithm).Assembly);
            builder.RegisterAssemblyTypes(typeof(PaymentsService).Assembly);
            builder.RegisterType<Random>();

            builder.RegisterType<DataGenerator>();
            //builder.RegisterType<PaymentsService>().AsSelf();
            //builder.RegisterType<ScheduleService>().AsSelf();

            //builder.RegisterType<MonthlyPaymentPartsMaker>().AsSelf();
            //builder.RegisterType<ScheduleItemDTOCollectionFactory>().AsSelf();
            //builder.RegisterType<ScheduleItemFactory>().AsSelf();
            builder.RegisterType<ScheduleFormer>().As<IScheduleFormer>();

            return builder.Build();
        }
    }
}
