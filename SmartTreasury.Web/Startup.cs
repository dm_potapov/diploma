﻿using Microsoft.Owin;
using SmartTreasury.Web;

[assembly: OwinStartup(typeof(Startup))]
namespace SmartTreasury.Web
{
    using System.Web.Http;
    using Microsoft.Owin;
    using Microsoft.Owin.Extensions;
    using Microsoft.Owin.FileSystems;
    using Microsoft.Owin.StaticFiles;
    using Owin;
    using Config;
    using Autofac.Integration.WebApi;

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var httpConfiguration = new HttpConfiguration();

            var container = AutofacConfig.Config();
            var dependencyResolver = new AutofacWebApiDependencyResolver(container);
            httpConfiguration.DependencyResolver = dependencyResolver;

            // Configure Web API Routes:
            // - Enable Attribute Mapping
            // - Enable Default routes at /api.
            httpConfiguration.MapHttpAttributeRoutes();
            httpConfiguration.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(httpConfiguration);

            //WebApiConfig.Register(httpConfiguration);

            app.UseWebApi(httpConfiguration);

            // Make ./public the default root of the static files in our Web Application.
            app.UseFileServer(new FileServerOptions
            {
                RequestPath = new PathString(string.Empty),
                FileSystem = new PhysicalFileSystem("./wwwroot"),
                EnableDirectoryBrowsing = true,
            });

            app.UseStageMarker(PipelineStage.MapHandler);
        }
    }
}
