﻿using SmartTreasury.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SmartTreasury.Main
{
    public class OptimizationCriteria
    {
        /// <summary>
        /// Критерий оптимизации - разница между поступлениями и исполненными заявками 
        /// за каждый определённый промежуток времени (чем больше, тем лучше)
        /// </summary>
        /// <returns></returns>
        public static decimal Compute(IEnumerable<PaymentApplication> paymentExecutions,
                            IEnumerable<ReceivedPayment> receivedPayments,
                            DateTime periodStart,
                            DateTime periodEnd)
        {
            // С начала (periodStart) до конца периода (periodEnd) с шагом в 1 день
            // Вычислять разницу между суммой поступлений и суммой исполняемых платежей
            // Даты у поступлений и платежей на каждой итерации в диапазоне (date, date + periodDaysStep),
            // где date получается из каждой предыдущей даты путём прибавления periodDaysStep
            // Полученные разницы сложить
            List<decimal> offsets = new List<decimal>();

            for (; periodStart < periodEnd.AddDays(-1); 
                   periodStart = periodStart.AddDays(1))
            {
                decimal executionSums = paymentExecutions
                                .Where(pa => pa.TreasureDate >= periodStart && pa.TreasureDate < periodStart.AddDays(1))
                                .Sum(pa => pa.Sum);
                decimal receivedPaymentsSums = receivedPayments
                                .Where(pa => pa.ReceiveDate >= periodStart && pa.ReceiveDate < periodStart.AddDays(1))
                                .Sum(pa => pa.PaymentSum);
                offsets.Add(receivedPaymentsSums - executionSums);
            }

            return offsets.Sum();
        }
    }
}
