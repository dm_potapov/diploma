﻿using MongoDB.Driver;
using Newtonsoft.Json;
using SmartTreasury.Models;
using SmartTreasury.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTreasury.Main
{
    class Program
    {
        static void Main(string[] args)
        {
            var applications = GetCollection<PaymentApplication>();
            foreach (var app in applications)
            {
                app.TreasureDate = app.DeadlineDate;
            }
            var receives = GetCollection<ReceivedPayment>();
            var creditLines = GetCollection<CreditLine>();

            CreditCalculator calc = new CreditCalculator(creditLines);
            PaymentsCalendarFactory factory = new PaymentsCalendarFactory(calc);

            PaymentsCalendar calendar = factory.Get(applications, receives,
                                DateTime.Parse("2017-01-01"), DateTime.Parse("2017-12-31"));
            Console.WriteLine(calendar.SumBalance);
            Console.ReadKey();
        }

        static ICollection<T> GetCollection<T>() where T : IEntity
        {
            var mongoDbRepository = new MongoDbRepository<T>(new MongoClient("mongodb://localhost:27017"),
                                                          "smartTreasury");
            return mongoDbRepository.ToList();
        }
    }
}
