﻿using Models.Calendar;
using SmartTreasury.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartTreasury.Main
{
    /// <summary>
    /// Кредитный калькулятор на основе доступных кредитных линий
    /// </summary>
    public class CreditCalculator
    {
        private readonly IEnumerable<CreditLine> _availableCreditLines;
        private readonly Dictionary<PaymentPeriod, int> _paymentPeriodDividers;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="availableCreditLines"></param>
        public CreditCalculator(IEnumerable<CreditLine> availableCreditLines)
        {
            if (availableCreditLines == null) throw new ArgumentNullException();

            _availableCreditLines = availableCreditLines;

            _paymentPeriodDividers = new Dictionary<PaymentPeriod, int>();
            _paymentPeriodDividers.Add(PaymentPeriod.Month, 30);
            _paymentPeriodDividers.Add(PaymentPeriod.Quarter, 90);
            _paymentPeriodDividers.Add(PaymentPeriod.Year, 360);
        }

        /// <summary>
        /// Попытка взять кредит
        /// </summary>
        /// <param name="takeDate">Дата заёма</param>
        /// <param name="sumToTake">Сумма заёма</param>
        /// <param name="daysPeriod">Период оплаты</param>
        /// <returns>
        /// Объект с результатом расчёта -  
        /// кодом статуса и (если есть возможность взять кредит) 
        /// составленным списком заявок на платежи с учётом процентов
        /// </returns>
        public CreditCalculateResult TryToGetCredit(DateTime takeDate, decimal sumToTake, int daysPeriod)
        {
            CreditCalculateResult result = new CreditCalculateResult();
            IEnumerable<CreditLine> availableLines = GetAvailableLines(takeDate, sumToTake, daysPeriod);
            if (availableLines.Count() == 0)
            {
                result.Status = CreditCalculateStatus.CantGetCredit;
            }
            else
            {
                result.Status = CreditCalculateStatus.CreditIsAvailable;
                var minimalPercentCreditLine = GetLineWithMinimalPercent(availableLines);
                result.CreditParts = CalculateCredit(minimalPercentCreditLine, takeDate, sumToTake, daysPeriod);
            }

            return result;
        }

        private IEnumerable<PaymentApplication> CalculateCredit(CreditLine minimalPercentCreditLine, 
                                                                DateTime takeDate, decimal sumToTake, 
                                                                int daysPeriod)
        {
            var resultPercent = getDividedPercent(minimalPercentCreditLine.Percent,
                                                  minimalPercentCreditLine.PercentPaymentPeriod)
                                                  * daysPeriod;

            sumToTake += (sumToTake * (decimal)(resultPercent / 100));

            int paymentsCount = daysPeriod / 30;
            decimal paymentSum = sumToTake / paymentsCount;

            DateTime nextDate = takeDate;

            for (int i = 0; i < paymentsCount; i++)
            {
                yield return new PaymentApplication()
                {
                    StartDate = nextDate,
                    DeadlineDate = nextDate.AddDays(30),
                    Sum = paymentSum,
                    Source = PaymentSource.Credit
                };
                nextDate = nextDate.AddDays(30);
            }
        }

        private CreditLine GetLineWithMinimalPercent(IEnumerable<CreditLine> availableLines)
        {
            return availableLines.Aggregate((cur, next) => (getDividedPercent(next.Percent, next.PercentPaymentPeriod)
                                                          < getDividedPercent(cur.Percent, cur.PercentPaymentPeriod))
                                                          ? next : cur);
        }

        private double getDividedPercent(double percent, PaymentPeriod divider)
        {
            return percent / _paymentPeriodDividers[divider];
        }

        private IEnumerable<CreditLine> GetAvailableLines(DateTime takeDate, decimal sumToTake, int daysPeriod)
        {
            return _availableCreditLines.Where(cl => cl.AvailabilityStartDate <= takeDate
                                        && cl.AvailabilityEndDate >= takeDate
                                        && cl.MaxSum >= sumToTake && cl.MinSum <= sumToTake
                                        && cl.MinDuration <= daysPeriod
                                        && cl.MaxDuration >= daysPeriod);
        }
    }
}
