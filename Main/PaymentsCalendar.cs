﻿using Models.Calendar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections;
using System.Linq.Expressions;

namespace SmartTreasury.Main
{
    public class PaymentsCalendar : IQueryable<PaymentsCalendarItem>
    {
        private readonly List<PaymentsCalendarItem> _items;

        public PaymentsCalendarItem this[string Date]
        {
            get
            {
                DateTime dateTimeParsed;
                if (DateTime.TryParse(Date, out dateTimeParsed))
                {
                    var item = _items.SingleOrDefault(d => d.Date.Date == dateTimeParsed.Date);
                    if (item == null)
                    {
                        throw new ArgumentOutOfRangeException();
                    }
                    return item;
                }
                else
                {
                    throw new ArgumentException("Incorrect date format!");
                }
            }
        }

        public void AddItem(PaymentsCalendarItem item)
        {
            _items.Add(item);
        }

        public decimal SumBalance
        {
            get
            {
                decimal resultSum = 0;
                foreach (var item in _items)
                {
                    resultSum += item.Balance;
                }

                return resultSum;
            }
        }

        public PaymentsCalendar()
        {
            _items = new List<PaymentsCalendarItem>();
        }

        public Type ElementType
        {
            get
            {
                return typeof(PaymentsCalendarItem);
            }
        }

        public Expression Expression
        {
            get
            {
                return _items.AsQueryable().Expression;
            }
        }

        public IQueryProvider Provider
        {
            get
            {
                return _items.AsQueryable().Provider;
            }
        }

        public IEnumerator<PaymentsCalendarItem> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _items.GetEnumerator();
        }
    }
}
