﻿using Models.Calendar;
using SmartTreasury.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartTreasury.Main
{
    public class PaymentsCalendarFactory
    {
        private readonly CreditCalculator _calculator;

        public PaymentsCalendarFactory(CreditCalculator calculator)
        {
            if (calculator == null) throw new ArgumentNullException();

            _calculator = calculator;
        }

        public PaymentsCalendar Get(ICollection<PaymentApplication> paymentApplications,
                            ICollection<ReceivedPayment> receivedPayments,
                            DateTime periodStart,
                            DateTime periodEnd)
        {
            AddCreditPaymentsToApplicationsIfNeeded(periodStart, periodEnd, 
                                                    ref paymentApplications, ref receivedPayments);

            PaymentsCalendar calendar = new PaymentsCalendar();

            for (; periodStart <= periodEnd;
                   periodStart = periodStart.AddDays(1))
            {
                var applicationsForDay = paymentApplications
                                .Where(pa => pa.DeadlineDate >= periodStart && pa.DeadlineDate < periodStart.AddDays(1))
                                .ToList();
                var receivesForDay = receivedPayments
                                .Where(pa => pa.ReceiveDate >= periodStart && pa.ReceiveDate < periodStart.AddDays(1))
                                .ToList();

                calendar.AddItem(new PaymentsCalendarItem(periodStart, receivesForDay, applicationsForDay));
            }

            return calendar;
        }

        private void AddCreditPaymentsToApplicationsIfNeeded
                                    (DateTime periodStart,
                                     DateTime periodEnd,
                                     ref ICollection<PaymentApplication> applications,
                                     ref ICollection<ReceivedPayment> receivedPayments)
        {
            var orderedReceives = receivedPayments.OrderBy(r => r.ReceiveDate).ToList();
            var prevDate = periodStart;
            foreach (var payment in orderedReceives)
            {
                var appsSum = applications.Where(a => a.DeadlineDate >= prevDate && a.DeadlineDate <= payment.ReceiveDate)
                                          .Sum(a => a.Sum);
                if (appsSum > payment.PaymentSum)
                {
                    var creditCalcResult = _calculator.TryToGetCredit(periodStart, appsSum - payment.PaymentSum,
                                                                (int)(payment.ReceiveDate - prevDate).TotalDays);
                    if (creditCalcResult.Status == CreditCalculateStatus.CreditIsAvailable)
                    {
                        receivedPayments.Add(new ReceivedPayment
                        {
                            Id = Guid.NewGuid(),
                            PaymentSum = appsSum - payment.PaymentSum,
                            ReceiveDate = periodStart,
                            Source = PaymentSource.Credit
                        });
                        applications = applications.Concat(creditCalcResult.CreditParts).ToList();
                    }
                }
                prevDate = payment.ReceiveDate;
            }
        }
    }
}
